A collection of config files, set up scripts.

# Linux
Linux configs files.
Files by default are located in home (`~`) directory, otherwise they should have `location: dir` at the top indicating file location.

# Windows
Windows scripts and config files.

# Nextcloud
Nextcloud Server commands for management.