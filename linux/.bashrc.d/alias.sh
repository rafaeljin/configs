# aliases

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

# avoid cp and rm to accidentally overwrite/remove file
alias cp='cp -i'
alias mv='mv -i'
alias rm='rm -i'

# alias which='alias | /usr/bin/which --tty-only --read-alias --show-dot --show-tilde'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# grep and du with frequently used options
alias mygrep="grep --include=*.{cpp,hpp,ipp,c,h,py} --color=always"
alias akgrep="mygrep --exclude-dir=test"
alias trgrep="mygrep --exclude-dir=tests --exclude=*.t.* --exclude=*.b.*"
alias mydu="du --max-depth=1 -h --exclude './.*' | sort -h"
alias myless="less -NR"

# ssh options
alias ssh="ssh -A -X"
s() {
	HOST=$(echo $1 | sed "s/inf/infradev/g")
	HOST=$(echo $HOST | sed "s/gti/gtidev/g")
	ssh $HOST
}

# xmllint
alias myxmllint="xmllint --noout --xinclude --nowarning --nsclean --schema"

# nextcloud occ
alias occ="sudo nextcloud.occ"

trvpn() {
	~/forticlientsslvpn/64bit/forticlientsslvpn_cli --server vpn.tower-research.com:443 --vpnuser rjin
}

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# I don't use this, but for compatibility purpose.
if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi
