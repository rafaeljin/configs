#!/bin/bash

# TODO check hosts file

function delete_dir {
	if [ -d "$1" ]; then 
		rm -r $1; 
	fi
}

function clear_dir {
	delete_dir $1
	mkdir $1
}

function check {
	DIR1=.
	DIR2=~

	TMP1=/tmp/confdir1
	TMP2=/tmp/confdir2
	clear_dir $TMP1
	clear_dir $TMP2

	for var in "$@"
	do
		# TODO perhaps introduce a patteron of .bashrc.d,specific.sh
		rsync -r $DIR1/$var $TMP1/$var --exclude=specific.sh
		rsync -r $DIR2/$var $TMP2/$var --exclude=specific.sh
	done

	diff -r $TMP1 $TMP2 --exclude=.*.sw*

	delete_dir $TMP1
	delete_dir $TMP2
}

# rename to check for testing
function checktest {
	for var in "$@"
	do
		echo $var
	done
}

BASH=(.bashrc .bashrc.d)
VIM=(.vimrc)
TMUX=(.tmux.conf)

# TODO make it one large script, delete dir at the end and take options here, .e.g. -q option and params to diff
case $1 in
	all)
		check ${BASH[@]} ${VIM[@]} ${TMUX[@]} .gitconfig
	;;
	vim)
		check ${VIM[@]}
	;;
	bash)
		check ${BASH[@]}
	;;
	tmux)
		check ${TMUX[@]}
	;;
	*)
		check $@
esac
# TODO filter out diff with sed to replace names
# TODO ignore swp tmp files and binaries

# Alternative:
#	https://stackoverflow.com/questions/10131908/only-include-files-that-match-a-given-pattern-in-a-recursive-diff
#	find $DIR1 $DIR2 -type f ! -name 'crazy' -printf '%f\n' | diff -r $DIR1 $DIR2 -X -

# TODO doesn't work with ./.bashrc.d/alias.sh

# TODO .gitignore specific and swps
