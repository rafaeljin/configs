""" For Unix: ~/.vimrc
""" For Windows: $VIM\_vimrc
""" :h 'specific setting' for help

""" ==========================================================================
""" vundle settings
set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" " alternatively, pass a path where Vundle should install plugins
" "call vundle#begin('~/some/path/here')
"
" " let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'
"
" " The following are examples of different formats supported.
" " Keep Plugin commands between vundle#begin/end.
" " plugin on GitHub repo
Plugin 'tpope/vim-fugitive'
" " plugin from http://vim-scripts.org/vim/scripts.html
" Plugin 'L9'
" " Git plugin not hosted on GitHub
Plugin 'git://git.wincent.com/command-t.git'
" " git repos on your local machine (i.e. when working on your own plugin)
" Plugin 'file:///home/gmarik/path/to/plugin'
" " The sparkup vim script is in a subdirectory of this repo called vim.
" " Pass the path to set the runtimepath properly.
Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
" " Install L9 and avoid a Naming conflict if you've already installed a
" " different version somewhere else.
Plugin 'ascenator/L9', {'name': 'newL9'}
"
" " All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" " To ignore plugin indent changes, instead use:
" "filetype plugin on
" "
" " Brief help
" " :PluginList       - lists configured plugins
" " :PluginInstall    - installs plugins; append `!` to update or just
" :PluginUpdate
" " :PluginSearch foo - searches for foo; append `!` to refresh local cache
" " :PluginClean      - confirms removal of unused plugins; append `!` to
" auto-approve removal
" "
" " see :h vundle for more details or wiki for FAQ
" " Put your non-Plugin stuff after this linekk



""" ==========================================================================
""" youCompleteMe configure file
let g:ycm_global_ycm_extra_conf = '~/.ycm_extra_conf.py'
""" jump through context errors
let g:ycm_always_populate_location_list = 1
Bundle 'Valloric/YouCompleteMe'


""" ==========================================================================
""" tmux-vim configure file
Bundle 'christoomey/vim-tmux-navigator'


""" ==========================================================================
""" repmo.vim - map last multiple lines jump to ';' 
" map a motion and its reverse motion:
:noremap <expr> h repmo#SelfKey('h', 'l')|sunmap h
:noremap <expr> l repmo#SelfKey('l', 'h')|sunmap l

" if you like `:noremap j gj', you can keep that:
:noremap <expr> j repmo#Key('gj', 'gk')|sunmap j
:noremap <expr> k repmo#Key('gk', 'gj')|sunmap k

" repeat the last [count]motion or the last zap-key:
:map <expr> ; repmo#LastKey(';')|sunmap ;
:map <expr> , repmo#LastRevKey(',')|sunmap ,

" add these mappings when repeating with `;' or `,':
:noremap <expr> f repmo#ZapKey('f')|sunmap f
:noremap <expr> F repmo#ZapKey('F')|sunmap F
:noremap <expr> t repmo#ZapKey('t')|sunmap t
:noremap <expr> T repmo#ZapKey('T')|sunmap T

" scroll command
:noremap <expr> <C-E> repmo#SelfKey('<C-E>', '<C-Y>')
:noremap <expr> <C-Y> repmo#SelfKey('<C-Y>', '<C-E>')



""" ==========================================================================
""" General Settings
set nocompatible 					" Use Vim settings instead of Vi settings. Has to be first command.
set backspace=indent,eol,start		" Make backspace behave in a sane manner.
set history=100						" vim default saves last 8 commands
filetype plugin indent on 			" Enable file type detection and do language-dependent indenting.
syntax on							" Turn on syntax feature	
set number							" show line number
set hidden							" Allow hidden buffers, don't limit to 1 file per window/split
set incsearch						""" setup search options
""" set hlsearch
set ts=4 sw=4						""" tab space is 4, shift width is 4.
set matchpairs+=<:> 				" Matching angle brackets in C++

""" full window
""" for windows
""" au GUIEnter * simalt ~x
"""set lines=999 columns=9999
"call system('wmctrl -i -b add,maximized_vert,maximized_horz -r '.v:windowid)

""" c++ std 11
" let g:syntastic_cpp_compiler = 'clang++'
let g:syntastic_cpp_compiler_options = ' -std=c++11 -stdlib=libc++'

""" spell check for markdown and latex
autocmd FileType latex,tex,md,markdown setlocal spell

