#!/bin/bash
# location: /etc/NetworkManager/dispatcher.d

LOG='/var/log/NetworkManager_dispatcher.log'
log () {
	echo "$(date +"%F %T") $1"  >> ${LOG}
}

[[ -z ${CONNECTION_UUID} ]] && exit 0

HOME_WIFI_UUID=296ab916-54e2-4f2b-923e-f911b179c52f

DEVICE=${1}
STATE=${2}

HOST_FILE="/etc/hosts"

# checking connection state is not necessary
if [[ ${CONNECTION_UUID} == ${HOME_WIFI_UUID} ]]; then
	HOST_FILE="/etc/hosts_home"
else
	HOST_FILE="/etc/hosts_regular"
fi

cp ${HOST_FILE} /etc/hosts
killall -HUP dnsmasq
log "Using ${HOST_FILE} as hosts file"

# DEBUG
log "Called with ($*) and connection uuid is: ${CONNECTION_UUID}"
